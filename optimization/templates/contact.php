<!DOCTYPE html>
<?php
include 'head.html';
include 'showMenu.php';
?>

<body>
  <?php include_once("analyticstracking.php") ?>
  <div class="container-narrow">

<?php
 echo showMenu("Contact");
?>
<hr>

<h4>Development</h4>

Examples are prepared by Vladislav V. Altsybeyev,
<br>
<a href="http://www.apmath.spbu.ru/ru/staff/altsyibeev/index.html">Associate lecturer of the Faculty of Applied Mathematics and Control Processes</a>
<br>
<br>
Contacts and information:
<br>
<li>Contact e-mail adress: altsybeyev@gmail.com</li>
<li><a href="https://www.scopus.com/authid/detail.uri?authorId=55890428700">Publications list in Scopus</a></li>
<li><a href="../index.php">List of another author projects</a></li>

<hr>
<h4>Bug reporting</h4>
If you found a possible bug in the code please send the corresponded bug description to the above contacts.

<br>
<br>


</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/modal.js"></script>
</body>