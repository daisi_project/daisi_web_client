<!DOCTYPE html>
{% load staticfiles %}

{% include 'head.html' %} 

    <body>

        <script>
            var fitness_type = 0;
            var is_run = false;
            var timer_plot;
            var chart;
        </script>

<div class="row">

    <div class="col-sm-1" id="left_">
    </div>

    <div class="col-sm-4" id="left">
    <h4>Problem:</h4>

    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select fitness
                        <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onClick="fitnessClick(0);">Rastrigin</a></li>
                                <li><a href="#" onClick="fitnessClick(1);">Rosenbrock</a></li>
                                <li><a href="#" onClick="fitnessClick(2);">Sphere</a></li>
                                <li><a href="#" onClick="fitnessClick(3);">Ackley</a></li>
                            </ul>
    </div>
    <br>
    <div id="fitness">
    <br>
    </div>
    <h4>Default config select:</h4>
    <label class="radio-inline"><input type="radio" name="optradio_cfg" onClick="configClick('{% static "optimization/data/gauss.json" %}');" checked>Gauss</label>
    <label class="radio-inline"><input type="radio" name="optradio_cfg" onClick="configClick('{% static "optimization/data/swarm.json" %}');">Particle Swarm</label>
    <label class="radio-inline"><input type="radio" name="optradio_cfg" onClick="configClick('{% static "optimization/data/ball.json" %}');">Heavy ball</label>
    <label class="radio-inline"><input type="radio" name="optradio_cfg" onClick="configClick('{% static "optimization/data/swarm_ball.json" %}');">Particle Swarm + Heavy ball</label>

    <br>
    <h4>Edit config:</h4>
    <br>

    <div id="editor"></div>
    </div>


    <div class="col-sm-4" id="right">

    <div id="container" style="width: 1050px; height: 340px; margin: 0 auto">

    <!-- <h4>System log</h4>

    <textarea class="form-control" rows="4" id="sys_log"></textarea>

    <h4>NOTK log</h4>

    <textarea class="form-control" rows="14" id="notk_log"></textarea> -->


                 
   </div>


<div class="row">
<div class="container">
        <button type="button" class="btn btn-primary" onClick="calculate();" id="calc_button">Calculate</button>
    </div>

</div>

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/boost.js"></script>

        <script src="{% static "optimization/js/loadXMLDoc.js" %}"></script>
        <script src="{% static "optimization/js/modal.js" %}"></script>

        <script src="{% static "daisi_app/lib/ace-builds/src-noconflict/ace.js" %}" type="text/javascript" charset="utf-8"></script>

        <script src="{% static "optimization/js/fitnessClick.js" %}"></script>
        <script src="{% static "optimization/js/configClick.js" %}"></script>
        <script src="{% static "optimization/js/plot.js" %}"></script>
        <script src="{% static "optimization/js/calculate.js" %}"></script>

        <script src="{% static "optimization/data/gauss.json" %}"></script>


        <script>
            var editor = ace.edit("editor");
            configClick('{% static "optimization/data/gauss.json" %}');
            // editor.setTheme("ace/theme/c_cpp");
            editor.getSession().setMode("ace/mode/json");
            fitnessClick(0);
        </script>
    </body>