from django.shortcuts import render
import subprocess
from django.http import JsonResponse
import os
import time

# Create your views here.


def main(request):
    return render(request, 'optimization.php')


app_path = '/home/user/projects/NOTK_TEST/build/release/web_example/notk_web_example'
log_path = '/var/log/notk_web/'


def start_notk(request):
    print('start_notk')
    cfg = request.GET.get('config')
    id = request.GET.get('id')
    log_level = request.GET.get('log_level')
    fitness_type = request.GET.get('fitness_type')
    time_expired = request.GET.get('time_expired')
    cfg_path = app_path + str(id) + '.cfg'
    f = open(cfg_path, "w")
    f.write(cfg)
    f.close()
    # url_log_notk = log_path + '/general_notk_' + str(id) + '.log'
    # url_log_system = log_path + 'log/system_' + str(id) + '.log'
    url_log_res = log_path + '/res_' + str(id) + '.log'

    app = app_path + ' ' + str(id) + ' ' + str(log_level) + ' ' + \
        str(fitness_type) + ' ' + str(time_expired) + ' ' + cfg_path
    print(app)

    subprocess.run(app, shell=True, check=True)
    data={}

    def read_data(url, tag):
        f = open(url_log_res, "r")
        data[tag]  = f.read()
        f.close()


    read_data(url_log_res, "res")
    # read_data(url_log_system, "sys")
    # read_data(url_log_notk, "notk")

    time.sleep(0.1)
    os.remove(url_log_res)
    # os.remove(url_log_system)
    # os.remove(url_log_notk)
    os.remove(cfg_path)

    return JsonResponse(data)
