from django.conf.urls import url
from . import views
from django.contrib.auth.forms import AuthenticationForm
from django.conf.urls import include
from django.contrib.auth import logout


urlpatterns = [
    url(r'^optimization$',
        views.main, name='main'),
    url(r'^$', views.main, name='main'),
    url(r'^start_notk$',
        view=views.start_notk, name='start_notk'),
]
