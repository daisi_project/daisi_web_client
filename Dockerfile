FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /daisi_web_client
WORKDIR /daisi_web_client
COPY requirements.txt .
RUN pip install -r requirements.txt

RUN apt-get update &&   \
apt-get install -q -y libboost-all-dev \
libpqxx-dev      \
libarmadillo-dev