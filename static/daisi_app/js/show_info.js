
function show_info(level, msg) {
  modal_body = document.getElementById("info_msg_body");
  modal_body.innerHTML = '';
  switch (level) {
    case 'success':
      modal_body.innerHTML +=
          '<div class="alert alert-success" role="alert">' + msg + '</div>';
      break;

    case 'error':
      modal_body.innerHTML +=
          '<div class="alert alert-danger" role="alert">' + msg + '</div>';
      break;
    default:
      modal_body.innerHTML +=
          '<div class="alert alert-info" role="alert">' + msg + '</div>';
  }

  $('#info_msg').modal("show");
}