function set_active(evt, class_name, parent_id, set_tag = " active") {
  if (parent_id != '') {
    btns =
        document.getElementById(parent_id).getElementsByClassName(class_name);
  } else {
    btns = document.getElementsByClassName(class_name);
  }
  for (i = 0; i < btns.length; i++) {
    btns[i].className = btns[i].className.replace(set_tag, "");
  }
  evt.currentTarget.className += set_tag;
}