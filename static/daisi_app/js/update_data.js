function set_data_sys(text) {
  var textarea = document.getElementById('curr_log');
  textarea.scrollTop = textarea.scrollHeight;
  textarea.innerHTML = text;
}

function set_data_log(data) {
  var textarea = document.getElementById('curr_log');

  textarea.scrollTop = textarea.scrollHeight;

  tt=String(data.log);

  var regex = new RegExp('!n', 'g');

  new_str='';

  for(i=0;i<data.len;i++)
  {
    if(tt[i]=='\\')
    {
      new_str +='!';
    }
    else
    {
      new_str+=tt[i];
    }
  }

  new_text = new_str.replace(regex, '&#13');

  textarea.innerHTML = new_text;
}

function update_data() {
  $.ajax({
    type: 'get',
    url: 'get_log_data',
    data: {},
    response: 'JSON',
    success: function(data) {
      set_data_log(data);
    }
  });
}
