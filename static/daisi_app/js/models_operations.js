var last_filter = 'label';
var last_is_descendr = true;

function show_full_description(descr) {
  show_info('info', descr);
}

function do_request(req_url, req_data, modal_id) {
  $.ajax({
    type: 'get',
    url: req_url,
    data: req_data,
    response: 'JSON',
    success: function(data) {
      $('#' + modal_id).modal('toggle');
      $("#" + modal_id + " .close").click();
      reload_models_list(last_filter, last_is_descendr);
      if (data.status) {
        show_info('success', 'Operation success');
      } else {
        show_info('error', 'Operation error: ' + data.message);
      }
    }
  });
}

function delete_model(tag, id) {
  var del_data = {'id': id};
  do_request('delete_model', del_data, tag + '_delete');
}

function clone_model(tag, id) {
  new_label = document.getElementById("label_" + tag).value;
  new_description = document.getElementById("description_" + tag).value;

  var clone_data =
      {'id': id, 'new_label': new_label, 'new_description': new_description};
  do_request('clone_model', clone_data, tag + '_clone');
}