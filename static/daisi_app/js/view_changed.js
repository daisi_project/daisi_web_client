function view_changed() {
  if (prev_component == '') {
    return;
  }
  var formData = new FormData();

  formData.append('component_pseudoname', prev_component);
  formData.append('view_pseudoname', prev_view);
  formData.append('component_name', prev_component_name);
  formData.append('is_solver', prev_is_solver);

  TextData = [];
  FilesIds = [];

  for (var [key, editor] of editors) {
    var tmp = {};
    tmp.id = key;
    tmp.content = editor.getValue();
    TextData.push(tmp);
  }

  for (var [key, file_] of files) {
    FilesIds.push(key);
    formData.append(key, file_.files[0]);
  }

  for (var [key, table] of vars_tables) {
    var var_data = '{';

    j_editors = table.getElementsByClassName("form-control");
    for (i = 0; i < j_editors.length; i++) {
      var key_editor = j_editors[i].id;
      var_data += '"' + key_editor + '": ' + j_editors[i].value;
      if (i != j_editors.length - 1) {
        var_data += ',';
      }
    }
    var_data += '}';
    var tmp = {};
    tmp.id = key;
    tmp.content = var_data;
    TextData.push(tmp);
  }

  for (var [key, table] of radio_groups) {
    var var_data = '{';

    j_editors1 = table.getElementsByClassName("radio");

    for (i = 0; i < j_editors1.length; i++) {
      j_editor = j_editors1[i];
      var key_editor = j_editor.value;
      var_data += '"' + key_editor + '": ' + j_editor.checked;
      if (i != j_editors1.length - 1) {
        var_data += ',';
      }
    }
    var_data += '}';
    var tmp = {};
    tmp.id = key;
    tmp.content = var_data;
    TextData.push(tmp);
  }

  formData.append('TextData', JSON.stringify(TextData));
  formData.append('FilesIds', JSON.stringify(FilesIds));
  formData.append('csrfmiddlewaretoken', getCookie('csrftoken'));

  try {
    $.ajax({
      type: 'post',
      url: 'set_view_content',
      data: formData,
      response: 'JSON',
      async: false,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {
        update_data();
        if (data.status == false) {
          show_info('error', 'Incorrect view data!');
        }
      }
    });
  } catch (e) {
    alert('Ошибка: ' + e.description);
  }
}
