function run(ii) {
  var Txt1 = "";
  var Txt2 = "";
  Txt1 = document.Test.bt.value;
  Txt2 = document.Test.bt.name;
  document.getElementById('ex1').innerHTML = "<HR>" + "Вы нажали кнопку: " +
      Txt1.bold() + " с именем: " + Txt2.bold() + "<HR>";
}


function loadXMLDoc(url, callback, show_err = true) {
  var req;

  req = null;
  if (window.XMLHttpRequest) {
    try {
      req = new XMLHttpRequest();
    } catch (e) {
    }
  } else if (window.ActiveXObject) {
    try {
      req = new ActiveXObject('Msxml2.XMLHTTP');
    } catch (e) {
      try {
        req = new ActiveXObject('Microsoft.XMLHTTP');
      } catch (e) {
      }
    }
  }

  req.timeout = 50000;
  if (req) {
    req.open("GET", url, true);
    req.onreadystatechange = function() {
      try {  // Важно!
        // только при состоянии "complete"


        if (req.readyState == 4) {
          // для статуса "OK"
          if (req.status == 200) {
            callback(req.responseText);

          } else {
            if (show_err) {
              return;
              alert(
                  "Не удалось получить данные: status != 200\n" +
                  req.statusText + " " + req.status.toString());
            }
          }
        }

      } catch (e) {
        if (show_err) {
          alert('Ошибка: ' + e.description);
        }
        // В связи с багом XMLHttpRequest в Firefox приходится отлавливать
        // ошибку
        // Bugzilla Bug 238559 XMLHttpRequest needs a way to report networking
        // errors
        // https://bugzilla.mozilla.org/show_bug.cgi?id=238559
      }
    };
    req.send(null);
  }
}

function set(text) {
  document.getElementById('ex2').innerHTML = text;
  hljs.initHighlighting.called = false;
  hljs.initHighlighting();
}

function btnClick() {
  // var txtFile = "./test.txt";

  // var timerId = setInterval(loadXMLDoc, 200,
  // 'http://localhost/num_meth/data/opimization/swarmdescend.json', set);

  // // через 5 сек остановить повторы
  // setTimeout(function() {
  //     clearInterval(timerId);
  //     alert('стоп');
  // }, 500000);


  //     var timerId =
  //     setInterval(loadXMLDoc('http://localhost/num_meth/test.txt'), 500);

  // setTimeout(function() {
  //   clearInterval(timerId);
  //   alert( 'стоп' );
  // }, 150000);
}