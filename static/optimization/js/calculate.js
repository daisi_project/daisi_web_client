
function set_data_notk_log(text) 
{
    var textarea = document.getElementById('notk_log');
    textarea.scrollTop = textarea.scrollHeight;
    textarea.innerHTML = text;
}

function set_data_sys(text) 
{
    var textarea = document.getElementById('sys_log');
    textarea.scrollTop = textarea.scrollHeight;
    textarea.innerHTML = text;
}

function update_data(url_log_notk, url_log_system)
{
    loadXMLDoc(url_log_notk, set_data_notk_log, false);
    loadXMLDoc(url_log_system, set_data_sys, false);
 //   chart.redraw(false);   
}

function calculate() 
{
    const randomBuffer = new Uint32Array(1);

    var id = window.crypto.getRandomValues(randomBuffer);

    // var timerId = setInterval(update_data, 200, url_log_notk, url_log_system);   

    //var timerIdch = setInterval(chart.redraw, 200, false);
  
    // plot('log/res_' + id.toString(10) + '.log');

    // $('#calc_button').prop('disabled', true);

    $.ajax({
        type: 'get',
        url: 'start_notk',
        data: {
            'fitness_type': fitness_type.toString(10),
            'config': editor.getValue(),
            'time_expired': '60',
            'log_level': '0',
            'id': id.toString(10)
        },
        response: 'text',
        success: function(data) 
        {
            // alert(data["res"]);

            plot(data["res"]); 
            // alert(data["res"]);
            // setTimeout( function(){
            // clearInterval(timerId);
           // clearInterval(timerIdch);
         //  chart.redraw(false);
            // plot_all('log/res_' + id.toString(10) + '.log', id);
        //   clearInterval(timer_plot);


            // $('#calc_button').prop('disabled', false);}, 500);
        // }
    }});
}