function fitnessClick(flag) {
    // document.getElementById('fitness').innerHTML= 
    fitness_type = flag;
    var elem = document.createElement("img");
    switch (flag) {
        case 0:
            elem.src = "http://latex.codecogs.com/gif.latex?f(\\mathbf%20{x}%20)=10n+\\sum%20_{i=1}^{n}\\left[x_{i}^{2}-10\\cos(2\\pi%20x_{i})\\right]\\rightarrow\\min. \\quad \\text{Solution: } f_{m}(\\mathbf {0} )=0";
            break;
        case 1:
            elem.src = "http://latex.codecogs.com/gif.latex?f(\\mathbf {x} )=\\sum _{i=1}^{N-1}\\left[(1-x_{i})^{2}+100(x_{i+1}-x_{i}^2)^2\\right]\\rightarrow\\min. \\quad \\text{Solution: } f_{m}(\\mathbf {1} )=0";
            break;
        case 2:
            elem.src = "http://latex.codecogs.com/gif.latex?f(\\mathbf {x} )=\\sum _{i=1}^{N}x_{i}^2\\rightarrow\\min. \\quad \\text{Solution: } f_{m}(\\mathbf {0} )=0";
            break;
        case 3:
            elem.src = "http://latex.codecogs.com/gif.latex?f(\\mathbf {x} )=-20\\exp{\\left(-0.2\\sqrt{\\frac{1}{N}\\sum_{i=1}^{N}x_{i}^2}\\right)}-\\exp{\\left(\\frac{1}{N}\\sum_{i=1}^{N}\\cos(2\\pi x_{i})\\right)}+20+\\exp{(1)}\\rightarrow\\min. \\quad \\text{Solution: } f_{m}(\\mathbf {0} )=0";
            break;
    }
    document.getElementById('fitness').innerHTML = "";
    document.getElementById('fitness').appendChild(elem);
}