var ii = 0;


function plot(text) {
  ii = 0;
  // Create the chart
  Highcharts.setOptions({global: {useUTC: false}});

  chart = Highcharts.chart('container', {
    boost: {useGPUTranslations: true},

    chart: {
      redraw: false,
      animation: {duration: 1},
      type: 'line',
      loading: {hideDuration: 0, showDuration: 0},
      events: {
          // load: function () {
          //     // set up the updating of the chart each second
          //     var series = this.series[0];
          //     timer_plot = setInterval(function ()
          //     {
          //         var data = [];
          //         loadXMLDoc(url, function (text) {

          //           data = text.split('\n');

          //           if(ii==0)
          //           {
          //                 var data_str = data[0].split(" ");
          //                 var cur_v =
          //                 parseFloat(data_str[data_str.length-1]);
          //                 series.addPoint([0,cur_v]);
          //           }
          //           for(var i=ii; i<data.length-1; i=i+1)
          //           {
          //             var data_str = data[i].split(" ");

          //             var cur_v =  parseFloat(data_str[data_str.length-1]);
          //             series.addPoint([i,cur_v], false);
          //           }
          //           this.redraw(false);
          //           ii = data.length-2;

          //          }, false);
          //         //this.redraw(false);
          //     }, 400);
          // }
      }
    },
    plotOptions: {
      line: {animation: {duration: 1}},
      series: {
        animation: {duration: 1},
        turboThreshold: 1e6,
        boostThreshold: 1e6,
        lineWidth: 4,
      }
    },
    title: {text: 'Fitness function values', style: {fontSize: 22}},
    xAxis: {
      labels: {style: {fontSize: 22}},
      title: {text: 'Iteration number', style: {fontSize: 22}},
    },
    yAxis: {
      type: 'logarithmic',

      title: {text: 'Fitness', style: {fontSize: 20}},
      plotLines: [{value: 0, width: 1, color: '#808080'}],
      labels: {style: {fontSize: 20}}
    },

    legend: {enabled: false},
    exporting: {enabled: false},
    series: [{name: 'Fitness function values', data: []}]
  });

  data = text.split('\n');

  for (var i = 0; i < data.length - 1; i = i + 1) {
    var data_str = data[i].split(' ');

    var cur_v = parseFloat(data_str[data_str.length - 1]);
    if (!isNaN(cur_v)) {
      // y_ar[i] = cur_v;
      chart.series[0].addPoint([i, cur_v], false);
    }
  }
  // chart.series[0].setData(y_ar);
  chart.redraw(false);
  done = true;

  //   chart.setData([0,1,2,3]);
  // chart.series[0].addPoint([0,1], false);
  // chart.series[0].addPoint([1,2], false);
  // chart.redraw(false);
}

function plot_(text) {
  // alert(text)
  data = text.split('\n');

  for (var i = 0; i < data.length - 1; i = i + 1) {
    var data_str = data[i].split(' ');

    var cur_v = parseFloat(data_str[data_str.length - 1]);
    if (!isNaN(cur_v)) {
      // y_ar[i] = cur_v;
      chart.series[0].addPoint([i, cur_v], false);
    }
  }
  // chart.series[0].setData(y_ar);
  chart.redraw(false);
  done = true;
}

function plot_all(url, id) {
  //   chart.setData([0,1,2,3]);
  var done = false;
  y_ar = [];
  loadXMLDoc(url, plot(text), false);
  // while(!done)
  // {}
}