"""daisi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^accounts/login/$', auth_views.LoginView.as_view(template_name='registration/login.html'),
        name="login"),

    url(r'^accounts/logged_out/$', auth_views.LogoutView.as_view(template_name='registration/logged_out.html'),
        name="logged_out"),

    url(r'^daisi/', include('daisi_app.urls')),
    # url(r'^image_processor/', include('image_processor.urls')),
    url(r'^logo_game/', include('logo_game.urls')),
    url(r'^invests/', include('invests.urls')),
    url(r'^optimization/', include('optimization.urls'))
]
