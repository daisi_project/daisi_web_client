from django.shortcuts import render
from django.http import JsonResponse
from . import models
from logo_game.models import Task

import json

# Create your views here.


def main(request):
    return render(request, 'main.html')

def get_task(request):
    number = json.loads(request.GET.get('number'))
    iteration = json.loads(request.GET.get('iteration'))

    result = {}
    result["status"] = False
    tasks = Task.objects.filter(number=number)

    if(tasks.count() < iteration):
        result["text"] = "Нет необходимого задания в базе для номера " + str(number)  + " и итерации " + str(iteration)   
        result["caption"] = "Ошибка"
    else:   
        result["text"] = tasks[iteration-1].text
        result["caption"] = tasks[iteration-1].caption

    return JsonResponse(result)
