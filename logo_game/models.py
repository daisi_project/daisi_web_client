from django.db import models

# Create your models here.
class Task(models.Model):
    text = models.TextField()
    number = models.IntegerField()
    caption = models.TextField(default='Задание')

