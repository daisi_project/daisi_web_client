from django.apps import AppConfig


class LogoGameConfig(AppConfig):
    name = 'logo_game'
