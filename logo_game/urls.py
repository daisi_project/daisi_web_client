from django.conf.urls import url
from . import views
from django.contrib.auth.forms import AuthenticationForm
from django.conf.urls import include
from django.contrib.auth import logout


urlpatterns = [
    url(r'^logo_game$',
        views.main, name='main'),
    url(r'^$', views.main, name='main'),

    url(r'^get_task/$',
        views.get_task, name='main'),
]
