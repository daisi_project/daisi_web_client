function show_concrete_view(
    evt, component_pseudoname, view_pseudoname, is_solver, name) {
  view_changed();
  prev_view = view_pseudoname;
  prev_component = component_pseudoname;
  prev_is_solver = is_solver;
  prev_component_name = name;

  editors.clear();
  files.clear();
  vars_tables.clear();
  radio_groups.clear();

  var tt = escape(name);

  $("#view_content")
      .html('')
      .load(
          "show_views?component_pseudoname=" + component_pseudoname +
              '&view_pseudoname=' + view_pseudoname + '&name=' + tt.toString(),
          function() {
            if (is_solver) {
              document.getElementById("btn_solve").innerHTML +=
                  '<button type="submit" class="btn btn-primary" onclick="add_result();">Run</button> ';
            }
          });

  set_active(evt, "tablinks", '');
}