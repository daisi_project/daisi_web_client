function show_views(evt, component_pseudoname, is_solver, name)
{   
   $("#view_content").html('');   

   set_active(evt, "list-group-item list-group-item-action", "left_menu");

  document.getElementById("views").innerHTML = '';
  var i = 0;

  {% for view in view_labels %}


  if ("{{view.component_type.pseudoname}}" == component_pseudoname)
  {
     var clic = "show_concrete_view(event, '{{view.component_type.pseudoname}}', '{{view.label.pseudoname}}'," + is_solver.toString() +", '"+name.toString()+ "')";
     document.getElementById("views").innerHTML += '<button class="tablinks" id="tablink_but" onClick="' + clic + '">{{ view.label.label}}</button>';
     if(i==0)
     {
        document.getElementById("tablink_but").click();
     }
     i++;
  }
  {% endfor %}
}