from django.shortcuts import render
from daisi_app.models import Models
from daisi_app.models import Modelcomponentsrelation
from daisi_app.models import Modelcomponentsviewsrelation
from daisi_app.models import Modelcomponentsviewssimpleitempattern
from daisi_app.models import Lazymodelcomponentsviewsitems
from daisi_app.models import Results
from daisi_app.models import Modelcomponentsviewsplotitempattern
from daisi_app.models import Plotdata
from daisi_app.models import Models
from daisi_app.models import Owners
from daisi_app.models import Textdata
from daisi_app import tools
from django.db.models import Q
from django.db import connections

from django.http import JsonResponse

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required


import json
import logging
import time
import inspect
import numpy
from operator import and_
from functools import reduce
import subprocess
import pandas as pd

# Create your views here.
# logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)

app_path = '/opt/daisi/daisiaccel_build/release/app/'
web_path = 'daisi_app/'
conn_file = app_path + 'config.json'

def get_owner(request):
    return Owners.objects.get(owner_name=request.user.get_username())


def add_axis(data, axis, label, font_label, font_ticks):
    result = data
    result[axis] = {}
    result[axis]["title"] = {}
    result[axis]["title"]["text"] = label
    result[axis]["title"]["style"] = {}
    result[axis]["title"]["style"]["fontSize"] = font_label
    result[axis]["labels"] = {}
    result[axis]["labels"]["style"] = {}
    result[axis]["labels"]["style"]["fontSize"] = font_ticks

    return result


def do_raw_request(request, args):
    data = {}
    data["status"] = True
    cursor = connections['daisi_db'].cursor()
    try:
        cursor.execute(request, args)
        result = cursor.fetchall()

    except Exception as err:
        data["status"] = False
        data["message"] = str(err)
        logger.error('Error perform raw request: ' + str(err))
        result = None
        return data, result

    return data, result


def request_view_to_items(request, pk, is_result_in):
    print('call request_view_to_items')
    component_pseudoname = request.GET.get('component_pseudoname')
    view_pseudoname = request.GET.get('view_pseudoname')
    name = request.GET.get('name')

    logger.debug(component_pseudoname)
    logger.debug(view_pseudoname)
    logger.info(name)

    status, res = do_raw_request("SELECT * FROM get_model_view_items_join_json(%s, %s, %s, %s, %s)", [
                                 pk, component_pseudoname, view_pseudoname, bool(is_result_in), name])
    if(status["status"] == False):
        raise "Error get view items"

    result = []

    for val in res:
        result.append(val[0])

    return result


def request_view_to_plot_items(request, is_result_in):
    return Modelcomponentsviewsplotitempattern.objects.filter(is_result=is_result_in,
                                                              component_type=request.GET.get('component_pseudoname'), view_label=request.GET.get('view_pseudoname'))


def get_models_list(request):
    filter = request.GET.get('filter', 'label')
    is_descend = request.GET.get('is_descend', 'true')
    logger.info(filter)

    if(get_owner(request).is_admin):
        models = Models.objects.all()
    else:
        models = Models.objects.filter(
            Q(owner_name__owner_name='Template') | Q(owner_name=get_owner(request)))

    models = models.filter(is_hidden=False)

    if is_descend == 'false':
        models = models.order_by(filter)
    else:
        models = models.order_by('-'+filter)

    return models


def get_results_list(request, pk):
    filter = request.GET.get('filter', 'start_time')
    is_descend = request.GET.get('is_descend', 'true')
    results = Results.objects.filter(model=pk)

    if is_descend == 'false':
        results = results.order_by(filter)
    else:
        results = results.order_by('-'+filter)

    return results


@login_required
def models_list(request):
    logger.info('models_list')
    return render(request, 'daisi_app/main.html', {'models': get_models_list(request), 'user': request.user.get_username()})


@login_required
def models_list_filtered(request):
    logger.info('models_list_filtered')
    return render(request, 'daisi_app/model_table.html', {'models': get_models_list(request), 'user': request.user.get_username()})


@login_required
def model_detail(request, pk):
    logger.info('call model_detail')
    model = Models.objects.get(id=pk)
    return render(request, 'daisi_app/model.html',
                  {'results': get_results_list(request, pk), 'components':  Modelcomponentsrelation.objects.filter(model_typename=model.typename),
                   'model': model, 'view_labels': Modelcomponentsviewsrelation.objects.all(),
                   'view_components_labels': Modelcomponentsviewsrelation.objects.all()})


@login_required
def show_result(request, pk):
    # logger.info('call show_result')
    # real_model_id = Results.objects.get(
    id=int(request.GET.get('result_id'))
    #return render(request, 'daisi_app/view.html')
    text_data = Textdata.objects.filter(results_id=id)
    return render(request, 'daisi_app/view.html', {'text_result': text_data,
                                                   'plot_components': request_view_to_plot_items(request, True)})


@login_required
def show_views(request, pk):
    logger.info('call show_views')
    return render(request, 'daisi_app/view.html', {'view_items': request_view_to_items(request, pk, False),
                                                   'plot_components': request_view_to_plot_items(request, False)})


@login_required
def get_log_file_name(request, is_current_op):
    name = str(request.session.session_key)
    if(is_current_op):
        name = name + '_current_op'
    else:
        name = name + '_problems'

    return name


def run_daisi(request, args):
    print('run daisi')
    data = {}

    app = app_path + "daisiaccel_app " + conn_file + " " + args

    try:
        print('run daisi 1')
        subprocess.run(app, shell=True, check=True)
        data["status"] = True

    except Exception as err:
        data["status"] = False
        data["message"] = "Error perform DAISI operation, see logs for detail"
        print(str(err))
        # logger.error(str(err))

    return data

def isfloat(value):
  try:
    return float(value)
  except ValueError:
    return float('nan')

@login_required
def set_view_content(request, pk):
    logger.info('call set_view_content')

    component_pseudoname = request.POST.get('component_pseudoname')
    is_solver = request.POST.get('is_solver')

    TextData = json.loads(request.POST.get('TextData'))
    FilesIds = json.loads(request.POST.get('FilesIds'))

    FilesData = request.FILES

    is_changed = False

    for item in TextData:
        print(int(item["id"]))
        print(item["content"])
        status, res = do_raw_request("SELECT * FROM check_lazy_item_data(%s, %s)",
                                     [int(item["id"]), item["content"]])
        if(res[0][0] == True):
            is_changed = True

    for item in FilesIds:
        file = FilesData.get(item, None)
        if file:
            df = pd.read_excel(file).values
            content = {}

            list_data_num = [[str(y) for y in x] for x in df]
            print( list_data_num)

            content["content"] =  list_data_num
            content["name"] = file.name
            status, res = do_raw_request("SELECT * FROM update_lazy_item_data(%s, %s)",
                                         [int(item), json.dumps(content)])
        else:
            status, res = do_raw_request("SELECT * FROM check_lazy_item_data(%s, %s)",
                                        [int(item), ""])
            if(res[0][0] == True):
                is_changed = True

    logger.info('is_changed: ' + str(is_changed))
    logger.info('is_solver: ' + str(is_solver))

    print('is_changed: ' + str(is_changed))
    print('is_solver: ' + str(is_solver))

    if(is_changed == True):
        print('copy')
        print(int(pk))
        print(is_solver)
        print( str(component_pseudoname))
        do_raw_request("SELECT * FROM copy_model_results(%s,%s,%s)",
                       [int(pk), is_solver, str(component_pseudoname)])

        for item in TextData:
            status, res = do_raw_request("SELECT * FROM update_lazy_item_data(%s, %s)",
                                         [int(item["id"]), item["content"]])

    result = {}
    if str(is_solver) == 'false':
        logger.info('run_daisi')
        last = request.POST.get('view_pseudoname')
        if last == '':
            last = 'empty_'

        result = run_daisi(request, "check_model_component "+str(request.user.get_username())+" "+str(pk)+" "+request.POST.get(
            'component_pseudoname') + " " + request.POST.get('component_name') + " " + last)
    else:
        logger.info('not run_daisi')
        result["status"] = True

    return JsonResponse(result)


@login_required
def get_plot_data(request, pk):
    logger.info('call get_plot_data')
    plot_id = request.GET.get('plot_id')
    result_id_clicked = request.GET.get('result_id_clicked')
    plot = Plotdata.objects.get(
        results_id=result_id_clicked, model_view_item_pattern=plot_id)

    data = {
        "legend": {
            "layout": 'vertical',
            "align": 'right',
            "verticalAlign": 'middle'
        },
        "plotOptions": {
            "series": {
                "marker": {
                    "enabled": 'false'
                }
            },
            "line": {
                "marker": {
                    "enabled": 'false'
                }
            }
        }
    }
    data["plotOptions"]["series"]["marker"]["enabled"] = False

    data["series"] = []

    for i in range(len(plot.xdata)):
        data_arr = [[]]
        for j in range(len(plot.xdata[i])):
            tmp = [plot.xdata[i][j], plot.ydata[i][j]]
            data_arr.append(tmp)

        if plot.is_standard == True:
            if len(plot.model_view_item_pattern.labels) == 0:
                name = ''
            else:
                name = plot.model_view_item_pattern.labels[i]
        else:
            name = plot.labels[i]

        width = 2
        marker = {
                "enabled": "false",
                "fillColor": 'transparent'
        }
        print(plot.model_view_item_pattern.format)

        if plot.model_view_item_pattern.format.typename == 'DottedPlot':
            width = 0
            marker =  {
                "radius": 2,
                "lineColor": '#666666',
                "lineWidth": 1
            }

        data["series"].append({
            "name": name,
            "marker": marker,
            "lineWidth": width,
            "data": data_arr
        })

    data["title"] = {}
    data["title"]["text"] = plot.model_view_item_pattern.item_label

    data = add_axis(
        data, "xAxis", plot.model_view_item_pattern.xlabel, 18, 18)
    data = add_axis(
        data, "yAxis", plot.model_view_item_pattern.ylabel, 18, 18)

    return JsonResponse(data)


@login_required
def clone_model(request):
    logger.info('call clone_model')

    first, second = do_raw_request("SELECT copy_model(%s, %s, %s, %s, %s, %s)",
                                   [int(request.GET.get('id')), int(get_owner(request).id), request.GET.get('new_label'), request.GET.get('new_description'), False, True])
    return JsonResponse(first)


@login_required
def delete_model(request):
    logger.info('call delete_model')
    id = int(request.GET.get('id'))
    logger.info(id)
    first, second = do_raw_request("SELECT delete_model(%s)", [id])
    return JsonResponse(first)


@login_required
def delete_result(request, pk):
    logger.info('call delete_result')
    first, second = do_raw_request("SELECT delete_results(%s)", [
                                   int(request.GET.get('result_id'))])
    return JsonResponse(first)


@login_required
def add_result(request, pk):
    logger.info('call add_result')

    return JsonResponse(run_daisi(request, "add_task " +str(request.user.get_username())+" "+ str(pk) + " " + request.GET.get('component_pseudoname') + " " + request.GET.get('component_name')))


@login_required
def show_results_list(request, pk):
    logger.info('call show_results_list')
    return render(request, 'daisi_app/results_table.html',
                           {'results': get_results_list(request, pk)})
 

@login_required
def get_log_data(request, pk):
    logger.info('call get_log_data')
    data={}
    val = str(Owners.objects.get(owner_name=request.user.get_username()).current_log)
    data["log"] = val
    data["len"] = len(val)
    return JsonResponse(data)

@login_required
def get_log_data_result(request, pk):
    logger.info('call get_log_data_result')
    data={}
    val = str(Results.objects.get(id=request.GET.get('result_id')).log)
    data["log"] = val
    data["len"] = len(val)
    return JsonResponse(data)


@login_required
def get_services_status(request, pk):
   logger.info('call get_services_status')

   first, second = do_raw_request("SELECT  * FROM get_services_list()",[])

   return JsonResponse(second, safe=False)

@login_required
def set_service_status(request, pk):
   logger.info('call set_services_status')
   first, second = do_raw_request("SELECT  * FROM set_service_status(%s, %s)",[request.GET.get('ip'), request.GET.get('status')])
   return JsonResponse(first)


@login_required
def reload_result(request, pk):
    result_id = request.GET.get('result_id')
    # print(result_id)
    result = Results.objects.get(id=int(result_id))
    result_resp = {}
    result_resp["status"] = result.status.name
    result_resp["result_id"] = result_id
    result_resp["is_rm"] = result.model.id == result.real_model.id
    result_resp["progress"] = result.ready_status
    result_resp["component_name"] = result.component_name.pseudoname
    result_resp["name"] = result.name
    return JsonResponse(result_resp)
