from django.apps import AppConfig


class DaisiAppConfig(AppConfig):
    name = 'daisi_app'
