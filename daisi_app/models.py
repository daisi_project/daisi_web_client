from django.db import models
from django.contrib.postgres.fields import ArrayField


class Modelcomponentsrelation(models.Model):
    model_typename = models.ForeignKey(
        'Modeltypesnames', models.DO_NOTHING, db_column='model_typename', primary_key=True)
    component_typename = models.ForeignKey(
        'Modelcomponentstypesnames', models.DO_NOTHING, db_column='component_typename')
    view_typename = models.ForeignKey(
        'Viewtypesnames', models.DO_NOTHING, db_column='view_typename')
    name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'modelcomponentsrelation'
        unique_together = (
            ('model_typename', 'component_typename', 'view_typename', 'name'),)


class Modelcomponentstypesnames(models.Model):
    pseudoname = models.TextField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'modelcomponentstypesnames'


class Modelcomponentsviewslabels(models.Model):
    pseudoname = models.TextField(primary_key=True)
    label = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'modelcomponentsviewslabels'


class Modelcomponentsviewsplotitempattern(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    component_type = models.ForeignKey(
        'Modelcomponentsviewsrelation', models.DO_NOTHING, db_column='component_type')
    view_label = models.ForeignKey(
        Modelcomponentsviewslabels, models.DO_NOTHING, db_column='view_label', blank=True, null=True)
    item_label = models.TextField()
    format = models.ForeignKey(
        'Plottypesnames', models.DO_NOTHING, db_column='format', blank=True, null=True)
    xlabel = models.TextField(blank=True, null=True)
    ylabel = models.TextField(blank=True, null=True)
    # This field type is a guess.
    labels = ArrayField(models.TextField(blank=True, null=True))
    is_editable = models.NullBooleanField()
    is_result = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'modelcomponentsviewsplotitempattern'
        unique_together = (('component_type', 'item_label'),)


class Modelcomponentsviewsrelation(models.Model):
    component_type = models.ForeignKey(
        Modelcomponentstypesnames, models.DO_NOTHING, db_column='component_type', primary_key=True)
    label = models.ForeignKey(
        Modelcomponentsviewslabels, models.DO_NOTHING, db_column='label')

    class Meta:
        managed = False
        db_table = 'modelcomponentsviewsrelation'
        unique_together = (('component_type', 'label'),)

    

class Modelcomponentsviewssimpleitempattern(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    component_typename = models.ForeignKey(
        Modelcomponentsviewsrelation, models.DO_NOTHING, db_column='component_typename')
    view_label = models.ForeignKey(
        Modelcomponentsviewslabels, models.DO_NOTHING, db_column='view_label')
    item_typename = models.ForeignKey('Modelcomponentsviewssimpleitemtypenames',
                                      models.DO_NOTHING, db_column='item_typename', blank=True, null=True)
    item_label = models.TextField()
    default_content = models.TextField(blank=True, null=True)
    is_editable = models.NullBooleanField()
    is_result = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'modelcomponentsviewssimpleitempattern'
        unique_together = (('component_typename', 'item_label', 'view_label'),)


class Modelcomponentsviewssimpleitemtypenames(models.Model):
    pseudoname = models.TextField(primary_key=True)
    typename = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'modelcomponentsviewssimpleitemtypenames'


class Models(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    label = models.TextField()
    description = models.TextField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    edited = models.DateTimeField(blank=True, null=True)
    is_hidden = models.NullBooleanField()
    typename = models.ForeignKey(
        'Modeltypesnames', models.DO_NOTHING, db_column='typename')
    lock_owner_name = models.ForeignKey('Owners', models.DO_NOTHING, db_column='lock_owner_name',
                                        blank=True, null=True, related_name="models_lock_owner_name")
    owner_name = models.ForeignKey(
        'Owners', models.DO_NOTHING, db_column='owner_name', related_name="models_owner_name")

    class Meta:
        managed = False
        db_table = 'models'
        unique_together = (('label', 'typename', 'owner_name'),)


class Lazymodelcomponentsviewsitems(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    model = models.ForeignKey('Models', models.DO_NOTHING)
    model_view_item_pattern_id = models.ForeignKey(
        'Modelcomponentsviewssimpleitempattern', models.DO_NOTHING, db_column='model_view_item_pattern_id')
    content = models.TextField(blank=True, null=True)
    is_valid = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'lazymodelcomponentsviewsitems'
        unique_together = (('model', 'model_view_item_pattern_id'),)


class Modeltypesnames(models.Model):
    pseudoname = models.TextField(primary_key=True)
    typename = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'modeltypesnames'


class Owners(models.Model):
    id = models.IntegerField(primary_key=True)
    owner_name = models.TextField(blank=True, null=True)
    is_admin = models.NullBooleanField()
    current_log = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'owners'



class Plottypesnames(models.Model):
    pseudoname = models.TextField(primary_key=True)
    typename = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plottypesnames'


class Resultstates(models.Model):
    pseudoname = models.TextField(primary_key=True)
    name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'resultstates'


class Results(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    model = models.ForeignKey(
        Models, models.DO_NOTHING, blank=True, null=True, related_name="results_model")
    component_name = models.ForeignKey(
        Modelcomponentstypesnames, models.DO_NOTHING, db_column='component_name', blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    done_time = models.DateTimeField(blank=True, null=True)
    ready_status = models.FloatField(blank=True, null=True)
    status = models.ForeignKey(
        'Resultstates', models.DO_NOTHING, db_column='status', blank=True, null=True)
    real_model = models.ForeignKey(
        Models, models.DO_NOTHING, blank=True, null=True, related_name="results_real_model")
    name = models.TextField(blank=True, null=True)
    log = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'results'

class Plotdata(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    model = models.ForeignKey(Models, models.DO_NOTHING)
    model_view_item_pattern = models.ForeignKey(
        Modelcomponentsviewsplotitempattern, models.DO_NOTHING)
    results = models.ForeignKey(Results, models.DO_NOTHING)
    xdata = ArrayField(ArrayField(models.FloatField(blank=True, null=True)))
    ydata = ArrayField(ArrayField(models.FloatField(blank=True, null=True)))
    is_standard = models.NullBooleanField()
    labels = ArrayField(models.TextField(blank=True, null=True))

    class Meta:
        managed = False
        db_table = 'plotdata'
        unique_together = (('model', 'model_view_item_pattern', 'results'),)


class Textdata(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    model = models.ForeignKey(Models, models.DO_NOTHING)
    model_view_item_pattern = models.ForeignKey(
        Modelcomponentsviewssimpleitempattern, models.DO_NOTHING)
    results = models.ForeignKey(
        Results, models.DO_NOTHING)
    data = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'textdata'
        unique_together = (('model', 'model_view_item_pattern', 'results'),)   



class Viewtypesnames(models.Model):
    pseudoname = models.TextField(primary_key=True)
    typename = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'viewtypesnames'
