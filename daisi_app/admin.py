from django.contrib import admin
from . import models

admin.site.register(models.Models)
admin.site.register(models.Owners)
# Register your models here.
