from django.conf.urls import url
from django.conf.urls import include
from . import views

urlpatterns = [
    url(r'^models_list_filtered/$',
        views.models_list_filtered, name='main'),
    url(r'^clone_model/$',
        views.clone_model, name='main'),
    url(r'^delete_model/$',
        views.delete_model, name='main'),
    url(r'^model/(?P<pk>\d+)/set_view_content$',
        views.set_view_content, name='model'),
    url(r'^model/(?P<pk>\d+)/show_views$', views.show_views, name='model'),
    url(r'^model/(?P<pk>\d+)/get_plot_data$',
        view=views.get_plot_data, name='model'),
    url(r'^model/(?P<pk>\d+)/show_result$', views.show_result, name='model'),
    url(r'^model/(?P<pk>\d+)/delete_result$',
        views.delete_result, name='delete_result'),
    url(r'^model/(?P<pk>\d+)/show_results_list$',
        views.show_results_list, name='show_results_list'),
    url(r'^model/(?P<pk>\d+)/get_log_data$',
        views.get_log_data, name='get_log_data'),
    url(r'^model/(?P<pk>\d+)/reload_result$',
        views.reload_result, name='reload_result'),
    url(r'^model/(?P<pk>\d+)/add_result$', views.add_result, name='add_result'),
    url(r'^model/(?P<pk>\d+)/$', views.model_detail, name='model'),
    url(r'^model/(?P<pk>\d+)/get_services_status', views.get_services_status, name='get_services_status'),
    url(r'^model/(?P<pk>\d+)/set_service_status', views.set_service_status, name='get_services_status'),
    url(r'^model/(?P<pk>\d+)/get_log_data_result', views.get_log_data_result, name='get_log_data_result'),
    url(r'^$', views.models_list, name='main'),
]
