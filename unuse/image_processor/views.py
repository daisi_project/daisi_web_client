from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

import logging

import time
import sys
import tensorflow as tf
import numpy as np
import json

from grpc.beta import implementations
from tensorboard._vendor.tensorflow_serving.apis import predict_pb2
from tensorboard._vendor.tensorflow_serving.apis import prediction_service_pb2
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions

import logging


def main(request):
    return render(request, 'main_image.html')


def preprocess_image(img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    return x


def get_prediction(host, port, img_path):
    image = preprocess_image(img_path)

    start_time = time.time()

    channel = implementations.insecure_channel(host, port)
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)
    request = predict_pb2.PredictRequest()
    request.model_spec.name = 'resnet50'
    request.model_spec.signature_name = 'predict'

    request.inputs['images'].CopyFrom(
        tf.contrib.util.make_tensor_proto(image, shape=image.shape))

    result = stub.Predict(request, 10.0)
    prediction = np.array(result.outputs['scores'].float_val)

    return prediction, (time.time()-start_time)*1000.


def process_image(request):
    print("call process_image")
    f = request.FILES['image']

    print(f)

    prediction, elapsed_time = get_prediction('127.0.0.1', 9001, f)

    result = decode_predictions(np.atleast_2d(prediction), top=3)[0]

    result_dict = {}

    for val in result:
        result_dict[val[1]] = val[2]

    print(result_dict)

    return JsonResponse(result_dict)


# def process_image(request):
#     print("call process_image")
#     result = {}
#     return JsonResponse(result)
