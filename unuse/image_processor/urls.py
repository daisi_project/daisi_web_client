from django.conf.urls import url
from . import views
from django.contrib.auth.forms import AuthenticationForm
from django.conf.urls import include
from django.contrib.auth import logout


urlpatterns = [
    url(r'^process_image$',
        views.process_image, name='main'),
    url(r'^$', views.main, name='main'),
]
