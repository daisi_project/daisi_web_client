#!/bin/bash

python3 ./manage.py migrate

python3 ./manage.py inspectdb --database=daisi_db &>> ./daisi_app/models.py

python3 ./manage.py makemigrations daisi_app

python3  ./manage.py migrate --database daisi_db --fake-initial daisi_app

