

# def str_time_to_timestamp(input):
#     return [datetime.datetime.strptime(time, '%Y-%m-%d').date() for time in input]

def str_time_to_timestamp(input):
    return datetime.datetime.strptime(input, '%Y-%m-%d')



def get_open_data(start_time_point, end_time_point, name):
    INDEX = int(0)
    PAGESIZE = int(0)
    TOTAL = int(1)

    datetime_arr = []
    data_arr = []

    while INDEX < TOTAL:
        uri = 'http://iss.moex.com/iss/history/engines/stock/markets/shares/securities/' + name + '.json?from=' + start_time_point + '&till=' + str(
            end_time_point) + '&start=' + str(INDEX)
        f = requests.get(uri)
        jres = json.loads(f.content)


        list_keys = ['OPEN', 'CLOSE', 'VOLUME', 'LOW', 'HIGH']

        indxes = [jres['history']['columns'].index(val) for val in list_keys]

        for val in jres['history']['data']:
            if 'TQBR' == val[0] or 'TQBS' == val[0] or val[0] == 'TQTF':
                data = []
                flag_add= True
                for indx in indxes:
                    if val[indx] is not None:
                        data.append(float(val[indx]))
                    else:
                        flag_add = False

                if flag_add:
                    print(val[1])
                    datetime_arr.append(str_time_to_timestamp(val[1]))
                    data_arr.append(data)

        cursor = jres['history.cursor']['data']

        PAGESIZE = cursor[0][2]
        INDEX = cursor[0][0] + PAGESIZE
        TOTAL = cursor[0][1]

    return CostData(datetime_arr, data_arr, name)
