# quandl for financial data
import quandl
# pandas for data manipulation
import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np 
import fbprophet
import models
import datetime
from fbprophet import Prophet
import plotly.graph_objects as go
from plotly.offline import plot
from scipy.signal import find_peaks

# Make some trendlines
import trendy





def supres(low, high, n=28, min_touches=2, stat_likeness_percent=1.5, bounce_percent=5):
    """Support and Resistance Testing
    Identifies support and resistance levels of provided price action data.
    Args:
        n(int): Number of frames to evaluate
        low(pandas.Series): A pandas Series of lows from price action data.
        high(pandas.Series): A pandas Series of highs from price action data.
        min_touches(int): Minimum # of touches for established S&R.
        stat_likeness_percent(int/float): Acceptable margin of error for level.
        bounce_percent(int/float): Percent of price action for established bounce.

    ** Note **
        If you want to calculate support and resistance without regard for
        candle shadows, pass close values for both low and high.
    Returns:
        sup(float): Established level of support or None (if no level)
        res(float): Established level of resistance or None (if no level)
    """
    import pandas as pd
    import numpy as np

    # Collapse into dataframe
    df = pd.concat([high, low], keys = ['high', 'low'], axis=1)
    df['sup'] = pd.Series(np.zeros(len(low)))
    df['res'] = pd.Series(np.zeros(len(low)))
    df['sup_break'] = pd.Series(np.zeros(len(low)))
    df['sup_break'] = 0
    df['res_break'] = pd.Series(np.zeros(len(high)))
    df['res_break'] = 0

    for x in range((n-1)+n, len(df)):
        # Split into defined timeframes for analysis
        tempdf = df[x-n:x+1]

        # Setting default values for support and resistance to None
        sup = None
        res = None

        # Identifying local high and local low
        maxima = tempdf.high.max()
        minima = tempdf.low.min()

        # Calculating distance between max and min (total price movement)
        move_range = maxima - minima

        # Calculating bounce distance and allowable margin of error for likeness
        move_allowance = move_range * (stat_likeness_percent / 100)
        bounce_distance = move_range * (bounce_percent / 100)

        # Test resistance by iterating through data to check for touches delimited by bounces
        touchdown = 0
        awaiting_bounce = False
        for y in range(0, len(tempdf)):
            if abs(maxima - tempdf.high.iloc[y]) < move_allowance and not awaiting_bounce:
                touchdown = touchdown + 1
                awaiting_bounce = True
            elif abs(maxima - tempdf.high.iloc[y]) > bounce_distance:
                awaiting_bounce = False
        if touchdown >= min_touches:
            res = maxima
        # Test support by iterating through data to check for touches delimited by bounces
        touchdown = 0
        awaiting_bounce = False
        for y in range(0, len(tempdf)):
            if abs(tempdf.low.iloc[y] - minima) < move_allowance and not awaiting_bounce:
                touchdown = touchdown + 1
                awaiting_bounce = True
            elif abs(tempdf.low.iloc[y] - minima) > bounce_distance:
                awaiting_bounce = False
        if touchdown >= min_touches:
            sup = minima
        if sup:
            df['sup'].iloc[x] = sup
        if res:
            df['res'].iloc[x] = res
    res_break_indices = list(df[(np.isnan(df['res']) & ~np.isnan(df.shift(1)['res'])) & (df['high'] > df.shift(1)['res'])].index)
    for index in res_break_indices:
        df['res_break'].at[index] = 1
    sup_break_indices = list(df[(np.isnan(df['sup']) & ~np.isnan(df.shift(1)['sup'])) & (df['low'] < df.shift(1)['sup'])].index)
    for index in sup_break_indices:
        df['sup_break'].at[index] = 1
    ret_df = pd.concat([df['sup'], df['res'], df['sup_break'], df['res_break']], keys = ['sup', 'res', 'sup_break', 'res_break'], axis=1)
    return ret_df


def rsiFunc(prices, n=14):
    deltas = np.diff(prices)
    seed = deltas[:n+1]
    up = seed[seed>=0].sum()/n
    down = -seed[seed<0].sum()/n
    rs = up/down
    rsi = np.zeros_like(prices)
    rsi[:n] = 100. - 100./(1.+rs)

    for i in range(n, len(prices)):
        delta = deltas[i-1] # cause the diff is 1 shorter

        if delta>0:
            upval = delta
            downval = 0.
        else:
            upval = 0.
            downval = -delta

        up = (up*(n-1) + upval)/n
        down = (down*(n-1) + downval)/n

        rs = up/down
        rsi[i] = 100. - 100./(1.+rs)

    return rsi


list_of_comp = ['FXIT']

start_time_point = '2017-10-22'

today = datetime.date.today()

end_time_point = str(today)

data = models.get_raw_data(start_time_point,end_time_point,list_of_comp)

template = data[0]

x = template.time()
y = template.close()

# Generate general support/resistance trendlines and show the chart
# winow < 1 is considered a fraction of the length of the data set
trendy.gentrends(y[-50:], window = 0.6, charts = True)

# Generate a series of support/resistance lines by segmenting the price history
#print(trendy.segtrends(y, segments = 2, charts = False))  # equivalent to gentrends with window of 1/2
#trendy.segtrends(y, segments = 3, charts = True)  # plots several S/R lines

# Generate smaller support/resistance trendlines to frame price over smaller periods
#trendy.minitrends(y, window = 30, charts = False)

# Iteratively generate trading signals based on maxima/minima in given window
#trendy.iterlines(y, window = 30, charts = True)  # buy at green dots, sell at red dots


xt = models.str_time_to_timestamp(x)

ser  =pd.Series(y, xt)

res = supres(ser, ser)

print(res)

plt.plot(res)
plt.show()


# peaks, _ = find_peaks(y, threshold = 30.5)

# pp = [y[p] for p in peaks]

# pd.series

# plt.plot(peaks, pp, "x")
# plt.show()
# predictions = 230


# d = {'ds': template.time(), 'y': template.close()}
# df = pd.DataFrame(data=d)

# train_df = df[:-predictions] 

# m = Prophet(changepoint_prior_scale=0.5)
# m.fit(train_df)
# future = m.make_future_dataframe(periods=predictions)
# forecast = m.predict(future)



#m.plot(forecast)
#plt.show()



# y_data = forecast.set_index('ds')['trend']
# y_data_low = forecast.set_index('ds')['trend_lower']
# y_data_high = forecast.set_index('ds')['trend_upper']

# axx=y_data.axes[0].tolist()
# ax = [val.to_pydatetime() for val in axx]



#print(ax)

# plt.plot(y_data)
# plt.plot(y_data_high)
# plt.plot(y_data_low.axes[0].tolist(), y_data_low.tolist())
# plt.plot(models.str_time_to_timestamp(template.time()[:-predictions]), template.close()[:-predictions])

# plt.show()

# cmp_df = forecast.set_index('ds')[['yhat', 'yhat_lower', 'yhat_upper']].join(df.set_index('ds'))

# cmp_df['e'] = cmp_df['y'] - cmp_df['yhat']
# cmp_df['p'] = 100*cmp_df['e']/cmp_df['y']
# print (np.mean(abs(cmp_df[-predictions:]['p'])))
# print (np.mean(abs(cmp_df[-predictions:]['e'])))

# show_forecast(cmp_df, predictions, 200)
