from django.shortcuts import render
from django.http import JsonResponse
from invests import models
from invests.models import ExtendedCostData
from invests import trendy
from invests.tools import get_series
from invests import finam_getter
from scipy.signal import find_peaks
from scipy import optimize

import json

import sys
import json
import urllib
import requests
import datetime
from collections import namedtuple
import numpy
from django.db import models
import pickle
from json import JSONEncoder
from django.core.cache import cache
from django.core import signals
from django.core.cache import _create_cache
import time
import copy
from datetime import timedelta


def get_raw_data(start_time_point,end_time_point,list_of_comp, period):
    data = []
    for val in list_of_comp:
        data.append(finam_getter.get_raw_data_finam(start_time_point, end_time_point, val, period))
        time.sleep(1.5)

    return data

def filter_growh(data):
    return[val for val in data if val.is_ideal_grow() is True]
            

def main(request):

    print ('call main')

    start_time_point = '2019-01-01'

    data_extended = []

    list_of_comp = ['CHMF', 'YNDX']
    list_of_comp = ['SBER','RSTI', 'LKOH', 'GAZP', 'YNDX', 'CHMF', 'MOEX', 'AFKS', 'GMKN', 'AFLT', 'GAZP', 'GRNT', 'LKOH', 'PLZL', 'PIKK']

   # list_of_comp = ['SBER', 'RSTI', 'LKOH']
   # list_of_comp = ['SBER']
   # list_of_comp = ['GAZP', 'YNDX', 'CHMF', 'AQUA']

    list_of_ma = [5, 20]



    today = datetime.date.today()

    end_time_point = str(today)

    start_time_point = str(today - timedelta(days=40) )   


    data = get_raw_data(start_time_point,end_time_point,list_of_comp, 'hour')

    template = copy.deepcopy(data[0])
    template.convert_norm_and_time()


    for val in data:
        data_extended.append(ExtendedCostData(val, template, list_of_ma))

    #data_extended = filter_growh(data_extended)

    for val in data_extended:
        val.calc_correl(data_extended)

    cache.set('data_extended',data_extended, 30000)

    return render(request, 'main_invests.html', {'data': data_extended, 'list_of_ma': list_of_ma})


def get_plot_data(request):
    list_of_days = [40, 20, 10, 5]
   # disable_list = [[0, 1, 5], [0, 1, 5], [4, 5], [4, 5], [3, 4, 5], [3, 4, 5]]
    disable_list = [[],[],[],[],[],[],[],[],[]]

    data_extended = cache.get('data_extended')
    plot_id = request.GET.get('plot_id')
    param_res = float(request.GET.get('res'))/100
    days_back_num = int(request.GET.get('days_back'))
    param_sup = float(request.GET.get('sup'))/100

    cache.set('data_extended',data_extended, 30000)

    days_back=8*list_of_days[days_back_num]


    for val in data_extended:
        # days_back = len(val.cost_data().time())

        if plot_id == val.cost_data().name():
            data = val.data_plot_ma()
            data["xAxis"]["min"] = data["xAxis"]["max"] - 86400*1000*list_of_days[days_back_num]
            for i in disable_list[days_back_num]:
                data["series"][i]["visible"] = False

            x = val.cost_data().time()[-days_back:]

            y1 = val.cost_data().low()[-days_back:]

            tt,tr1 = trendy.gentrends(y1, window = param_sup, charts = False)  

            y2 = val.cost_data().high()[-days_back:]

            tr2,tt = trendy.gentrends(y2, window = param_res, charts = False)  

            data["yAxis"][0]["min"] = min(y1)*0.99
            data["yAxis"][0]["max"] = max(y1)*1.01

            def app(app_data, name):
                data["series"].append({
                            "marker": {
                                "enabled": "false",
                                "fillColor": 'transparent'
                            },
                            "dashStyles": 'LongDash',
                            "color": 'black',
                            "lineWidth": 1,
                            "name": name,
                            "data": get_series(x, app_data)
                        })


          

            def get_slope(y, nparts, is_min):
                step = int(len(y)/nparts)
                start = 0

                extr_inds = []          

                for part in range(nparts):
                    end = start + step

                    if end == len(y) - 2 or end > len(y) - 1:
                        end = len(y) - 1

                    subarr = y[start:end]    

                    if is_min is False:
                        extr_inds.append(start + subarr.index(max(subarr)))
                    else:
                        extr_inds.append(start + subarr.index(min(subarr)))
                     
                    start = end + 1

                def make_line(x):
                    xar = numpy.linspace(0, len(y), len(y))
                    # print(xar)
                    yar = x[0]*xar+x[1]
                    return yar

                def fitness(x):
                    yar = make_line(x)    
                    fit = 0
                    for ind in extr_inds:
                        fit = fit + (y[ind] - yar[ind])*(y[ind] - yar[ind])

                    for i in range(0,len(yar)):
                        if (is_min is False and y[i]-0.0001>yar[i]) or (is_min is True and y[i]+0.0001<yar[i]):
                                fit = fit + (y[i]-yar[i])*(y[i]-yar[i])
                    return fit

                bounds = [(-1, 1), (-1, 1)]

                results = optimize.dual_annealing(fitness, bounds)

                return make_line(results['x'])

            tr2 = get_slope(y2, 3, False)
            tr1 = get_slope(y1, 3, True)

            # maxslope = (x[max1] - x[max2]) / (max1 - max2)  # slope between max points
            # minslope = (x[min1] - x[min2]) / (min1 - min2)  # slope between min points
            # a_max = x[max1] - (maxslope * max1)  # y-intercept for max trendline
            # a_min = x[min1] - (minslope * min1)  # y-intercept for min trendline
            # b_max = x[max1] + (maxslope * (len(x) - max1))  # extend to last data pt
            # b_min = x[min1] + (minslope * (len(x) - min1))  # extend to last data point
            # maxline = np.linspace(a_max, b_max, len(x))  # Y values between max's
            # minline = np.linspace(a_min, b_min, len(x))  # Y values between min's


            app(tr1, "support")
            app(tr2, "resistance")

            return JsonResponse(data)
