from django.db import models

import requests
import json
import datetime
import numpy
import time

from invests import tools


# Create your models here.
class CostData:
    def __init__(self, time, data, name):
        """Constructor"""
        self.__time = time
        self.__open = []
        self.__close = []
        self.__volume = []
        self.__low = []
        self.__high = []
        self.__name = name

        for val in data:
            self.__open.append(val[0])
            self.__close.append(val[1])
            self.__volume.append(val[2])
            self.__low.append(val[3])
            self.__high.append(val[4])

    def convert_norm_and_time(self):
        base = self.__close[0]
        self.__open = [val / base for val in self.__open]
        self.__close = [val / base for val in self.__close]
        self.__low = [val / base for val in self.__low]
        self.__high = [val / base for val in self.__high]



    def get_candle_series(self):
        data_arr = []
        for j in range(len(self.__time)):
            tmp = [tools.to_high_chart_timestamp(
                self.__time[j]), self.__open[j], self.__high[j], self.__low[j], self.__close[j]]
            data_arr.append(tmp)

        return data_arr

    def low(self):
        return self.__low

    def high(self):
        return self.__high

    def get_volume_series(self):
        return tools.get_series(self.__time, self.__volume)

    def time(self):
        return self.__time

    def open(self):
        return self.__open

    def close(self):
        return self.__close

    def name(self):
        return self.__name

    def volume(self):
        return self.__volume

    def get_series(self):
        return tools.get_series(self.__time, self.__close)


class MAdata:
    def __init__(self, cost_data, ma, prev_y):
        """Constructor"""
        self.__time = cost_data.time()[ma:]
        self.__ma = ma
        self.__values = tools.partial_sum(ma, cost_data.close())
        self.__is_grow = False

        sign = prev_y[-1] - self.__values[-1]

        if(sign > 0):
            self.__is_grow = True

        self.__change_date = self.__time[0]

        delta = len(prev_y) - len(self.__values)

        for i in range(len(self.__values) - 2, 0, -1):
            if sign * (prev_y[i+delta] - self.__values[i]) < 0:
                self.__change_date = self.__time[i]
                break

    def get_series(self):
        return tools.get_series(self.__time, self.__values)

    def time(self):
        return self.__time

    def values(self):
        return self.__values

    def ma(self):
        return self.__ma

    def is_grow(self):
        return self.__is_grow

    def change_date(self):
        return self.__change_date


class ExtendedCostData:

    def calc_correl_coef(self, template_cost_data):
        arr1 = []
        arr2 = []
        for i in range(0, len(self.__cost_data.time()), 1):
            try:
                ind = template_cost_data.time().index(
                    self.__cost_data.time()[i])
                arr1.append(self.__cost_data.close()[i])
                arr2.append(template_cost_data.close()[ind])
            except:
                continue

        print(numpy.corrcoef(arr1, arr2)[0, 1])
        return round(numpy.corrcoef(arr1, arr2)[0, 1], 2)

    def calc_correl(self, all_data):
        self.__corells = []
        for data in all_data:
            self.__corells.append(self.calc_correl_coef(data.cost_data()))

    def corells(self):
        return self.__corells

    def __init__(self, cost_data, template_cost_data, list_of_ma):
        """Constructor"""
        self.__diff = round(
            100*(cost_data.close()[-1]-cost_data.close()[-2])/cost_data.close()[-1], 2)
        self.__cost_data = cost_data
        self.__cost_data.convert_norm_and_time()
        self.__correl_coef = self.calc_correl_coef(template_cost_data)
        last_time = tools.to_high_chart_timestamp(self.__cost_data.time()[-1])

        self.__ma_data = []
        prev_y = self.__cost_data.close()

        self.__data_plot_ma = {
            "legend": {
                "enabled": "true",
                "layout": 'vertical',
                "align": 'right',
                "verticalAlign": 'middle'
            },

            # "rangeSelector": {
            #             "buttons": [
            #                 {"type": 'day', "count": 5, "text": '1m'},
            #                 {"type": 'day', "count": 20, "text": '5m'},
            #                 {"type": 'day', "count": 50, "text": '15m'},
            #                 {"type": 'all', "text": 'All'}
            #             ],
            #             "selected": 0
            # },

            "plotOptions": {
                "series": {
                    "marker": {
                        "enabled": 'false'
                    }
                },
                "line": {
                    "marker": {
                        "enabled": 'false'
                    }
                }
            },
            "xAxis": {
                "type": "datetime",
                "labels": {
                        "format": "{value:%Y-%m-%d}"
                },
                "max": last_time,
                "min": 0,
            },
            "yAxis": [{
                "labels": {
                    "align": 'right',
                    "x": -3
                },
                "title": {
                    "text": 'OHLC'
                },
                "height": '60%',
                "lineWidth": 2,
                "resize": {
                    "enabled": True
                },
                "max": 0,
                "min": 0

            }, {
                "labels": {
                    "align": 'right',
                    "x": -3
                },
                "title": {
                    "text": 'Volume'
                },
                "top": '65%',
                "height": '35%',
                "offset": 0,
                "lineWidth": 2
            }]
        }

        self.__data_plot_ma["plotOptions"]["series"]["marker"]["enabled"] = False

        self.__data_plot_ma["series"] = []

        for val in list_of_ma:
            self.__ma_data.append(MAdata(self.__cost_data, val, prev_y))

            self.__data_plot_ma["series"].append({
                "name": "ma " + str(val),
                "marker": {
                    "enabled": "false",
                    "fillColor": 'transparent'
                },
                'showInLegend': 'true',
                "lineWidth": 1,
                "data": self.__ma_data[-1].get_series()
            })
            prev_y = self.__ma_data[-1].values()

        self.__data_plot_ma["series"].append({
            "name": cost_data.name(),
            "marker": {
                "enabled": "false",
                "fillColor": 'transparent'
            },
            "data": self.__cost_data.get_series()
        })

        self.__data_plot_ma["series"].append({
            "name": cost_data.name(),
            "type": 'candlestick',
            "data": self.__cost_data.get_candle_series()
        })

        self.__data_plot_ma["series"].append({
            "type": 'column',
            "name": 'Volume',
            "yAxis": 1,
            "data": self.__cost_data.get_volume_series(),
        })

        self.__is_ideal_grow = True

        for ma in self.__ma_data[1:]:
            if ma.is_grow() is False:
                self.__is_ideal_grow = False
                break
    
    def is_ideal_grow(self):
        return self.__is_ideal_grow

    def cost_data(self):
        return self.__cost_data

    def ma_data(self):
        return self.__ma_data

    def diff(self):
        return self.__diff

    def data_plot_ma(self):
        return self.__data_plot_ma

    def correl_coef(self):
        return self.__correl_coef

    def pack(self):
        return [self.__diff, self.__correl_coef, [val.pack() for val in self.__ma_data], self.__cost_data.pack(), self.__diff]
