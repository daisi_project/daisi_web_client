from invests import models
import datetime
import time
import numpy


def normalize_data(input):
    return [val/input[0] for val in input]


def partial_sum(n, input):
    res = numpy.empty(len(input) - n)
    for i in range(n, len(input), 1):
        res[i-n] = numpy.sum(input[i-n:i]) / n
    return res


def to_high_chart_timestamp(input):
    return 1000*time.mktime(input.timetuple())


def get_series(x, y):
    data_arr = [[]]
    for j in range(len(x)):
        tmp = [to_high_chart_timestamp(x[j]), y[j]]
        data_arr.append(tmp)
    return data_arr
