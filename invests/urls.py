from django.conf.urls import url
from . import views
from django.contrib.auth.forms import AuthenticationForm
from django.conf.urls import include
from django.contrib.auth import logout


urlpatterns = [
    url(r'^invests$',
        views.main, name='main'),
    url(r'^$', views.main, name='main'),
    url(r'^get_plot_data$',
        view=views.get_plot_data, name='get_plot_data'),
]
