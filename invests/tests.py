from django.test import TestCase

import finam_getter
import datetime
import models
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy


def gauss(x, *p):
    A, mu, sigma = p
    return A*numpy.exp(-(x-mu)**2/(2.*sigma**2))

# start_time_point = '2015-01-01'
# end_time_point = '2018-01-01'


def get_gauss(data):
    p0 = [1., 0., 1.]

    hist, bin_edges = numpy.histogram(data, bins=500, density=True)
    bin_centres = (bin_edges[:-1] + bin_edges[1:])/2

    coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
    return coeff


start_time_point = '2018-01-01'

# start_time_point = '2015-01-01'
end_time_point = '2019-09-26'
# end_time_point = '2018-01-01'


# today = datetime.date.today()
# end_time_point = str(today)

ticker = 'SI'
period = 'min'

# end_time_point = str(today)

# models.get_open_data(start_time_point, end_time_point, ticker)

data = finam_getter.get_raw_data_finam(
    start_time_point, end_time_point, ticker, period)

# opens = data.open()
# closes = data.close()

# bul = []
# bear = []


# for i in range(2, len(opens)-3, 1):
#     delta = (closes[i+2] - closes[i])/closes[i]*100
#     if(closes[i] > opens[i] and closes[i-1] < opens[i-1] and closes[i-2] < opens[i-2] and opens[i] < closes[i-1] and closes[i]> opens[i-1]):
#         bul.append(delta)
#     else:
#         bear.append(delta)


# fig, (ax1, ax2) = plt.subplots(2, 1)

# ax1.hist(bul, 500, density=True, facecolor='g', alpha=0.75)
# ax2.hist(bear, 500, density=True, facecolor='g', alpha=0.75)

# print(sum(bul)/len(bul))
# print(sum(bear)/len(bear))

# # print(get_gauss(bul))

# # print(get_gauss(bear))


# plt.show()
